import React, { useState } from 'react';
import Modal from './components/Modal';
import Button from './components/Button';
import ModalImage from './components/ModalImage';
import ModalText from './components/ModalText';

function App() {
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);
  const [buttonsVisible, setButtonsVisible] = useState(true);

  const openFirstModal = () => {
    setFirstModalOpen(true);
    setButtonsVisible(false);
  };

  const openSecondModal = () => {
    setSecondModalOpen(true);
    setButtonsVisible(false);
  };

  const closeFirstModal = () => {
    setFirstModalOpen(false);
    setButtonsVisible(true);
  };

  const closeSecondModal = () => {
    setSecondModalOpen(false);
    setButtonsVisible(true);
  };

  return (
    <>
      <div>
        {buttonsVisible && <Button onClick={openFirstModal}>Open first modal</Button>}
        {buttonsVisible && <Button onClick={openSecondModal}>Open second modal</Button>}

        <Modal isOpen={firstModalOpen} onClose={closeFirstModal}>
          <ModalImage isOpen={firstModalOpen} onClose={closeFirstModal} imageUrl="your-image-url" />
        </Modal>

        <Modal isOpen={secondModalOpen} onClose={closeSecondModal}>
          <ModalText isOpen={secondModalOpen} onClose={closeSecondModal} text="Your modal text" />
        </Modal>
      </div>
    </>
  );
}

export default App;