import React from 'react';
import Modal from './Modal';

export default function ModalText ({ isOpen, onClose }) {

  const handleButtonClick = () => {
    
    onClose();
  };

  return (

    
    <Modal isOpen={isOpen} onClose={onClose}>
      <Modal.Header>
        <Modal.Close onClick={onClose} />
      </Modal.Header>
      <Modal.Body>
        
        <h1>Add Product “NAME”</h1>
        <p>Description for you product.</p>
        <button onClick={() => handleButtonClick(1)}>ADD TO FAVORITE</button>
      </Modal.Body>
    </Modal>
  );
};
