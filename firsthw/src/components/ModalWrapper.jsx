import React from 'react';
import './ModalWrapper.scss';

export default function ModalWrapper({ children, onClose }) {
  return (
    <div className="modal-wrapper" onClick={onClose}>
      {children}
    </div>
  );
};





