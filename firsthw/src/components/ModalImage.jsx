import React from 'react';
import Modal from './Modal';

export default function ModalImage({ isOpen, onClose }) {
    const imageUrl = 'https://i.ytimg.com/vi/OpFGDUOCgBI/hqdefault.jpg';

    const handleButtonClick = (buttonIndex) => {
        if (buttonIndex === 1) {
            // Логіка для першої кнопки
        } else if (buttonIndex === 2) {
            // Логіка для другої кнопки
            onClose(); // Закрити модальне вікно після кліку на другу кнопку
        }
    };

    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <Modal.Header >
                <Modal.Close onClick={onClose} />
            </Modal.Header>
            <Modal.Body>
                <img src={imageUrl} alt="Modal Image" style={{ maxHeight: '200px' }} />
                <h1>Product Delete!</h1>
                <p>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
                <button className='btn' onClick={() => handleButtonClick(1)}>First Button</button>
                <button onClick={() => handleButtonClick(2)}>Close Modal</button>
            </Modal.Body>
        </Modal>
    );
};

