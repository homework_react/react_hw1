import React from 'react';
import './Button.scss';

export default function Button({ type, classNames, onClick, children }) {
  return (
    <button type={type} className={`button ${classNames}`} onClick={onClick}>
      {children}
    </button>
  );
};
