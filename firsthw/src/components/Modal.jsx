import React from 'react';
import ModalWrapper from './ModalWrapper';
import ModalHeader from './ModalHeader';
import ModalFooter from './ModalFooter';
import ModalClose from './ModalClose';
import ModalBody from './ModalBody';
import './Modal.scss';

const Modal = ({ children, isOpen, onClose }) => {
  return (
    <>
      {isOpen && (
        <ModalWrapper onClose={onClose}>
          <div className="modal">
            {React.Children.map(children, child => {
              if (React.isValidElement(child)) {
                return React.cloneElement(child);
              }
              return child;
            })}
          </div>
        </ModalWrapper>
      )}
    </>
  );
};

Modal.Header = ModalHeader;
Modal.Footer = ModalFooter;
Modal.Close = ModalClose;
Modal.Body = ModalBody;

export default Modal;