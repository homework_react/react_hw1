import React from 'react';

export default function ModalClose ({ onClick }) {
  return <span className="modal-close" onClick={onClick}>&times;</span>;
};



